module Esonero.OrWeakAnd where

open import Library.Logic
open import Library.Logic.Laws

{- 4 - PROPRIETA' ∨-weak-∧

   ESERCIZIO 4-1

   Si dimostri la seguente proprietà dei connettivi ∧ e ∨:
-}

∨-weak-∧ : ∀{A B C : Set} → (A ∨ B) ∧ C → A ∨ (B ∧ C)
∨-weak-∧ (inl x , y) = inl x
∨-weak-∧ (inr x , y) = inr (x , y)

{- ESERCIZIO 4-2

   Si consideri l'implicazione inversa di ∨-weak-∧:

   A ∨ (B ∧ C) → (A ∨ B) ∧ C

   Se ne dimostri la validità, se del caso, ovvero la si refuti
   producendo un controesempio consitente in un'opportuna scelta
   A', B', C' tali che:

   ¬ (A' ∨ (B' ∧ C') → (A' ∨ B') ∧ C')

   dimostrando quest'ultimo asserto.

   controesempio A = ⊤ , B = C = ⊥  

-}
es2 : ¬ ( ⊤ ∨ (⊥ ∧ ⊥) → (⊤ ∨ ⊥) ∧ ⊥)
es2 x = snd (x (inl <>))
