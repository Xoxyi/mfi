module Esonero.Filter where

open import Library.Nat
open import Library.Bool
open import Library.Equality
open import Library.LessThan
open import Library.List

{- 3 - LUNGHEZZA DELLE LISTE PRODOTTE DA filter

   ESERCIZIO 3-1

   Si definisca la funzione:
-}

filter : ∀{A : Set} → (A → Bool) → List A → List A
filter p [] = []
filter p (x :: xs) with p x
filter p (x :: xs) | true = x :: (filter p xs)
filter p (x :: xs) | false = filter p xs

{- che data una funzione booleana su A, p : A → Bool ed una lista xs : List A
   restituisce la lista degli x in xs tali che p x == true, nell'ordine relativo
   in cui sono elencati in xs.
-}


{- ESERCIZIO 3-2

   Usando la relazione ≤, definita come <= nella libreria LessThan:
-}

infix 4 _≤_

_≤_ : ℕ → ℕ → Set
x ≤ y = x <= y

{- si dimostri il teorema: -}

length-filter : ∀{A : Set} → (p : A → Bool) → (xs : List A) →
                length (filter p xs) ≤ length xs

length-filter p [] = le-zero
length-filter p (x :: xs) with p x
length-filter p (x :: xs) | true = le-succ (length-filter p xs)
length-filter p (x :: xs) | false = le-trans (length-filter p xs) (aux (length xs))
  where
    aux : ∀(x : ℕ) → x ≤ succ x
    aux zero = le-zero
    aux (succ x) = le-succ (aux x) 


