module Esonero.Bin where

open import Library.Nat
open import Library.Nat.Properties
open import Library.Equality
open import Library.Equality.Reasoning

{- 1 - STRINGHE BINARIE

   Si consideri il tipi di dato Bin per la rappresntazione dei naturali
   in notazione binaria:
   
-}

data Bin : Set where
  ⟨⟩ : Bin
  _O : Bin → Bin
  _I : Bin → Bin

{- Ad esempio, la stringa binaria 1011, che rappresenta il numero undici, è codificata come

⟨⟩ I O I I

Le rappresentazioni con stringhe binarie non sono uniche a causa degli zeri a sinistra,
che possono essere in numero arbitrario: anche la stringa 001011 ovvero la sua codifica

⟨⟩ O O I O I I

rappresnta lo stesso numero di 1011.
-}

{-  ESERCIZIO 1-1

Si definisca la funzione

inc : Bin → Bin

che converte una stringa binaria in una di quelle che rappresntano il numero naturale successivo
a quello rappresntatato dalla stringa data.

Ad esmpio, poché 1100 codifica il numero dodici, dovremmo avere

inc (⟨⟩ I O I I) ≡ ⟨⟩ I I O O
-}

inc : Bin → Bin
inc ⟨⟩ = ⟨⟩ I
inc (b O) = b I
inc (b I) = (inc b) O

{- Quindi, usando inc si definiscano le funzioni -}

to : ℕ → Bin
to zero = ⟨⟩ O
to (succ n) = inc (to n)

from : Bin → ℕ
from ⟨⟩ = 0
from (b O) = (from b) * 2
from (b I) = succ ((from b) * 2)

{- La prima trasforma un naturale in una stringa binaria che lo rappresenti;
   la seconda effettua la trasformazione inversa.
   Nella definizione di to si rappresnti lo zero con ⟨⟩ O, metre in quella di from
   si assegni zero anche alla stringa ⟨⟩.

   Verificate con C-c C-n alcuni esempi di tali funzioni.
-}

{- ESERCIZIO 1-2

   Si considerino le seguenti equazioni, in cui n : ℕ e b : Bin:

   from (inc b) == suc (from b)
   to (from b) == b
   from (to n) == n

   Per ciascuna di esse si fornisca una dimostrazione, se vale, oppure
   un controesempio in caso contrario.
-}

es1 : ∀(b : Bin) → from (inc b) == succ (from b)
es1 ⟨⟩ = refl
es1 (b O) = refl
es1 (b I) rewrite es1 b = refl

{-
  non vale
  controesempio to (from ⟨⟩) == to 0 == ⟨⟩ 0 != ⟨⟩
-}
es2 : ∀(b : Bin) → to (from b) == b
es2 b = {!!}

es3 : ∀(n : ℕ) → from (to n) == n
es3 zero = refl
es3 (succ n) rewrite es1 (to n) | es3 n = refl


